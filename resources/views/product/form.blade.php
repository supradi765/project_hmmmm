@extends('layout.app')

@section('title', 'Product')

@if(session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif
@section('content')

<form action="#" method="POST" >
    @csrf
    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 fontweight-bold text-primary">Form Tambah Product</h6>
                </div>
                <div class="card-body">
                        <div class="form-group">
                            <label for="cumi">cumi dan kepiting</label>
                            <input type="text" class="form-control" name="kategori_barang" id="kategori_barang" value="{{ old('kategori_barang') }}" required/>
                        </div>
                        <div class="form-group">
                            <label for="sayuran">sayuran</label>
                            <input type="text" class="form-control" name="kategori_barang" id="kategori_barang" value="{{ old('kategori_barang') }}" required/>
                        </div>
                        <div class="form-group">
                            <label for="ikan">ikan</label>
                            <input type="text" class="form-control" name="kategori_barang" id="kategori_barang" value="{{ old('kategori_barang') }}" required/>
                        </div>
                        <div class="form-group">
                            <label for="minuman"> minuman</label>
                            <input type="text" class="form-control" name="kategori_barang" id="kategori_barang" value="{{ old('kategori_barang') }}" required/>
                        </div>
                        <div class="form-group">
                            <label for="happy_hour">happy hour</label>
                            <input type="text" class="form-control" name="kategori_barang" id="kategori_barang" value="{{ old('kategori_barang') }}" required/>
                        </div>
                        <div class="form-group">
                            <button type="submit">Simpan</button>
                        </div>
                 </div>
            </div>
        </div>
    </div>
</form>

@endsection

