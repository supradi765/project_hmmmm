@extends('layout.app')

@section('title', 'Table Barang')

@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
@if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif

@section('content')


    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <a href="{{route('product.form')}}" class="btn btn-secondary"><i class="fas fa-plus"></i> <span>Tambah Product</span></a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered gambar" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <td>No</td>
                            <td class="text-center">
                                <img src="{{ asset('asset/gambar/cumi_dan_kerang.jpg') }}" alt="ini gambar">
                                <br>
                                cumi dan kerang
                            </td>
                            <td class="text-center"> <img src="{{ asset('asset/gambar/sayuran.jpg') }}" alt="sayuran">
                                <br>
                                sayuran
                            </td>
                            <td class="text-center"><img src="{{ asset('asset/gambar/ikan.jpg') }}" alt="ikan">
                                <br>
                                ikan
                            </td>
                            <td class="text-center"><img src="{{ asset('asset/gambar/minuman.jpeg') }}" alt="minuman">
                                <br>
                                minuman
                            </td>
                            <td class="text-center">
                                <img src="#" alt="harga">
                                <br>
                                happy hour
                            </td>
                            <td class="text-center">
                                <img src="{{ asset('asset/gambar/edit.jpg') }}" alt="">
                                <br>
                                Aksi
                            </td>
                        </tr>
                    </thead>
                        @php
                            $no=1;
                        @endphp
                    <tbody>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
