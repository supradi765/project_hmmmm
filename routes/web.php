<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::controller(ProductController::class)->name('product.')->prefix('product')->group(function () {
    Route::get('/product', 'index')->name('product');
    Route::get('/form', 'formInput')->name('form');
  //  Route::post('/orders', 'store');
});
