<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'product';

    public $timestamp = true;

    protected $fillable = [
        'cumi',
        'sayuran',
        'ikan',
        'minuman',
        'happy_hour',
    ];
}
